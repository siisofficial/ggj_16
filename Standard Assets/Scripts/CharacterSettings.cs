﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.SocialPlatforms;
using UnityEngine.UI;

namespace UnityStandardAssets {
    public class CharacterSettings : MonoBehaviour {

        public enum Hero {
            Alper,
            Ibrahim,
            Mustafa
        };


        public Hero ThisHero;

        public float defFireDamage1 = 10;

        public float defFireDamage2 = 20;

        public float defArmor = 0;

        public float defSpeed = 100;

        public float defHealth = 100;

        public Slider healthBar;


        [Range(0, 100)]
        public float fireDamage1;

        [Range(0, 100)]
        public float fireDamage2;

        [Range(0, 100)]
        public float armor;

        [Range(0, 100)]
        public float speed;

        [Range(0, 100)]
        public float health;


        private float initHealth;


        // Use this for initialization
        void Start () {
            defFireDamage1 += fireDamage1;
            defFireDamage2 += fireDamage2;
            defArmor += armor;
            defSpeed += speed;
            defHealth += health;

            initHealth = defHealth;
        }

        void Update() {
            healthBar.value = defHealth / initHealth;

        }

    }
}
