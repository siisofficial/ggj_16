﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets._2D;

namespace UnityStandardAssets {
	public class damageController : MonoBehaviour {

        public string ThisName;
        public CharacterSettings CharSet;
        public bool attack1;

		// Use this for initialization
		void OnTriggerEnter (Collider GO) {
            if((GO.name == "Mustafa" || GO.name == "Alper" || GO.name == "Ibrahim") && GO.name != ThisName) {
                if(attack1) {
                    GO.GetComponent<PlatformerCharacter2D>().TakeDamage(CharSet.defFireDamage1);
                } else {
                    GO.GetComponent<PlatformerCharacter2D>().TakeDamage(CharSet.defFireDamage2);
                }
            }
		}

        void disable() {
            gameObject.SetActive(false);
        }
	}
}