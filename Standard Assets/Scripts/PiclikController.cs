﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;
using UnityEngine.UI;
using UnityStandardAssets;
using UnityStandardAssets._2D;

public class PiclikController : MonoBehaviour {

    public List<String> piclikList;
    public PlatformerCharacter2D PC2D_p1;
    public PlatformerCharacter2D PC2D_p2;
    public CharacterSettings CS_p1;
    public CharacterSettings CS_p2;
    public Animator cam_p1;
    public Animator cam_p2;
    public Transform player1;
    public Transform player2;
    public float targetPiclikTime = 30f;

    private static System.Random rng = new System.Random();

    private bool isMirrorMirror = false;
    private bool isFearnShake = false;

    private float passedTime = 0;
    private bool isWaitingForPiclik = true;
    private int funcIdx = 0;

    public string nextElder = "Burak";
    private string activeElder = "";

    [Header("Canvas Elements")]
    public Image SliderHandle;
    public Transform Burak;
    public Transform Utku;
    public Sprite burakMini;
    public Sprite utkuMini;
    public Slider slider;

    public Text yazi1;
    public Text yazi2;

    private void Shuffle(IList<String> list) {
        int n = list.Count;
        while (n > 1) {
            n--;
            int k = rng.Next(n + 1);
            String value = list[k];
            list[k] = list[n];
            list[n] = value;
        }
    }



    public void CustomFunction(String functionName) {
        Type thisType = this.GetType();
        MethodInfo theMethod = thisType.GetMethod(functionName);
        theMethod.Invoke(this, null);

    }


    // Use this for initialization
    void Start () {
        Shuffle(piclikList);

    }

    // Update is called once per frame
    void Update () {
        if (isWaitingForPiclik) {
            passedTime += Time.deltaTime;
            slider.value = passedTime / targetPiclikTime;

            if (activeElder == "") {
                Burak.localRotation = Quaternion.Euler(0, 0, 270);
                Utku.localRotation = Quaternion.Euler(0, 0, 270);
            }
        }

        if (passedTime >= targetPiclikTime && funcIdx < piclikList.Count) {
            CustomFunction(piclikList.ToArray()[funcIdx]);
            funcIdx++;
            isWaitingForPiclik = false;
            passedTime = 0;
            slider.value = 0;
            SliderHandle.enabled = false;
            StartCoroutine("ResetPiclikler");
            activeElder = nextElder;
        }

        if (isMirrorMirror) {
            PC2D_p1.flipper = -1;
            PC2D_p2.flipper = -1;
        } else if (isFearnShake) {
            cam_p1.SetBool("hasShake", true);
            cam_p2.SetBool("hasShake", true);
        } else {
            cam_p1.SetBool("hasShake", false);
            cam_p2.SetBool("hasShake", false);
            PC2D_p1.flipper = 1;
            PC2D_p2.flipper = 1;
        }

        if (!isWaitingForPiclik) {
            if (activeElder == "Burak") {
                Burak.localRotation = Quaternion.Euler(0, 0, 0);
                Utku.localRotation = Quaternion.Euler(0, 0, 270);
            } else if (activeElder == "Utku") {
                Burak.localRotation = Quaternion.Euler(0, 0, 270);
                Utku.localRotation = Quaternion.Euler(0, 0, 0);
            } else if (activeElder == "") {
                Burak.localRotation = Quaternion.Euler(0, 0, 270);
                Utku.localRotation = Quaternion.Euler(0, 0, 270);
            }
        }
    }



    public void GoldenGoal() {
        yazi1.text = "Golden Goal Mode Activated!";
        yazi2.text = "Golden Goal Mode Activated!";
        CS_p1.defHealth = 1;
        CS_p2.defHealth = 1;
    }

    public void MirrorMirror() {
        yazi1.text = "Mirror Mirror Mode Activated!";
        yazi2.text = "Mirror Mirror Mode Activated!";
        isMirrorMirror = true;
    }

    public void Fear_N_Shake() {
        yazi1.text = "Fear'n Shake Mode Activated!";
        yazi2.text = "Fear'n Shake Mode Activated!";
        isFearnShake = true;
    }

    public void Vector3_Swap() {
        yazi1.text = "Vector3.Swap();";
        yazi2.text = "Vector3.Swap();";
        Vector3 _temp = player1.position;

        player1.position = player2.position;
        player2.position = _temp;
    }


    IEnumerator ResetPiclikler() {
        yield return new WaitForSeconds(11);

        isFearnShake = false;
        isMirrorMirror = false;
        SliderHandle.enabled = true;

        isWaitingForPiclik = true;
        activeElder = "";
        if (nextElder == "Burak") {
            nextElder = "Utku";
            SliderHandle.sprite = utkuMini;
        } else {
            nextElder = "Burak";
            SliderHandle.sprite = burakMini;
        }
    }

    IEnumerator ResetPicliklerMini() {
        yield return new WaitForSeconds(5);

        SliderHandle.enabled = true;

        isWaitingForPiclik = true;
        activeElder = "";
        if (nextElder == "Burak") {
            nextElder = "Utku";
        } else {
            nextElder = "Burak";
        }
    }
}
