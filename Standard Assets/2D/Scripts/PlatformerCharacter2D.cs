using Mono.Xml.Xsl;
using System;
using System.Collections;
using UnityEngine;
using UnityStandardAssets;

namespace UnityStandardAssets._2D {
    public class PlatformerCharacter2D : MonoBehaviour {

        public Animator characterAnimator;

        public GameObject yerCatlama;

        [Range(0, 10)]
        public float CoolDown = 4;
        private float CoolDownPassedTime;

        private bool canAttack;


        [SerializeField]
        private float m_MaxSpeed = 10f;                    // The fastest the player can travel in the x axis.

        [SerializeField]
        public CharacterSettings CharacterSetting;


        private Rigidbody m_Rigidbody;
        public bool m_FacingRight = false;  // For determining which way the player is currently facing.


        public GameObject pewObject;


        public Vector3 direction;

        private Vector3 LastPos;

        public GameController GC;

        public bool isDead = false;

        public Renderer SR;

        public CapsuleCollider mCollisdsd;

        public int flipper = 1;

        //Burak ATEŞ ETMECE
        //        public GameObject Bullet;
        //        GameObject bulletClone;
        //
        //        public float shotForce = 1000;
        //        public float coolDown = 1;
        //
        //
        //        float fire;
        //        float shotTimer = 0;
        //----------------------------------


        void Update() {
            //BURAK ekledi.. shotTimer'ı arttırmak için.
            //            shotTimer += Time.deltaTime;
            if (!canAttack) {
                CoolDownPassedTime += Time.deltaTime;
            }

            if (CoolDownPassedTime > CoolDown) {
                canAttack = true;
                CoolDownPassedTime = 0;
            }

        }


        private void Awake() {

            m_Rigidbody = GetComponent<Rigidbody>();
            canAttack = true;
            CoolDownPassedTime = 0;

            direction = new Vector3(1.0f, 0, 0);
            if (m_FacingRight) direction = new Vector3(-1, 0, 0);
            LastPos = gameObject.transform.position;
        }



        private void FixedUpdate() {



        }

        public void Fire1() {

            if (!canAttack || GC.LevelFinished) {
                return;
            }
            if (!characterAnimator.GetCurrentAnimatorStateInfo(0).IsName("Attack1")) {
                characterAnimator.SetTrigger("attacking1");

                canAttack = false;
                switch (CharacterSetting.ThisHero) {
                    case CharacterSettings.Hero.Alper:
                        StartCoroutine("pewActive");
                        break;
                    case CharacterSettings.Hero.Ibrahim:
                        pewObject.SetActive(true);
                        break;
                    case CharacterSettings.Hero.Mustafa:
                        StartCoroutine("pewActive2");
                        break;
                    default:
                        break;
                }
            }

        }

        public void Fire2() {


            if (!canAttack || GC.LevelFinished) {
                return;
            }

            if (!characterAnimator.GetCurrentAnimatorStateInfo(0).IsName("Attack2")) {
                characterAnimator.SetTrigger("attacking2");
                canAttack = false;
            }

            switch (CharacterSetting.ThisHero) {
                case CharacterSettings.Hero.Alper:
                    alpoAttack();
                    break;
                case CharacterSettings.Hero.Ibrahim:
                    iboAttack();
                    break;
                case CharacterSettings.Hero.Mustafa:
                    break;
                default:
                    break;
            }
        }

        public void enableChildren() {
            yerCatlama.SetActive(true);


        }

        public void MakeDamage(GameObject GO, float Damage) {       //  @TODO:  Sanırım Bunun damage verecek GameObject'in script'inde tanımlanması daha doğru.
            PlatformerCharacter2D PC2DScript = GO.GetComponent<PlatformerCharacter2D>();

            PC2DScript.TakeDamage(Damage);
        }

        public void TakeDamage(float Damage) {
            if (!GC.LevelFinished) {
                CharacterSetting.defHealth -= Damage;

                m_Rigidbody.velocity = new Vector3(0, 0, 0);

                characterAnimator.SetTrigger("takingDamage");
                CheckHealth();
            }
        }

        public void CheckHealth() {
            if (!GC.LevelFinished) {
                Debug.Log("Health Bar güncelle: " + CharacterSetting.defHealth);
                if (CharacterSetting.defHealth <= 0) Die();
            }
        }

        public void Die() {
            if (!GC.LevelFinished) {
                isDead = true;
                characterAnimator.SetTrigger("die");
                GC.FinishGame();
            }
        }

        public void Move(float horizontalMove, float verticalMove) {

            if ((characterAnimator.GetCurrentAnimatorStateInfo(0).IsName("Idle") || characterAnimator.GetCurrentAnimatorStateInfo(0).IsName("Run")) && !GC.LevelFinished ) {

                horizontalMove *= flipper;
                verticalMove *= flipper;
                // Move the character
                m_Rigidbody.velocity = new Vector3(horizontalMove * m_MaxSpeed, verticalMove * m_MaxSpeed, 0);


                if (Math.Abs(m_Rigidbody.velocity.x) > 0.1f || Math.Abs(m_Rigidbody.velocity.y) > 0.1f ) {
                    characterAnimator.SetBool("hasSpeed", true);

                    direction = LastPos - gameObject.transform.position;

                    direction = direction.normalized;
                    LastPos = gameObject.transform.position;
                } else {
                    characterAnimator.SetBool("hasSpeed", false);
                }

                // If the input is moving the player right and the player is facing left...
                if (horizontalMove > 0 && !m_FacingRight)
                {
                    Flip();
                }
                    // Otherwise if the input is moving the player left and the player is facing right...
                else if (horizontalMove < 0 && m_FacingRight)
                {
                    Flip();
                }
            }

        }


        private void Flip() {
            // Switch the way the player is labelled as facing.
            m_FacingRight = !m_FacingRight;

            // Multiply the player's x local scale by -1.
            Vector3 theScale = transform.localScale;
            theScale.x *= -1;
            transform.localScale = theScale;
        }

        private void iboAttack() {

        }

        private void alpoAttack() {

        }

        private void mustoAttack() {
            Color clr = SR.material.color;
            clr.a = 0.4f;
            SR.material.color = clr;
            mCollisdsd.enabled = false;

            StartCoroutine("OpacityFix");
        }

        IEnumerator OpacityFix() {
            yield return new WaitForSeconds(4);
            Color clr = SR.material.color;
            clr.a = 1.0f;
            SR.material.color = clr;
            mCollisdsd.enabled = true;
        }

        IEnumerator pewActive() {
            yield return new WaitForSeconds(0.6f);
            pewObject.SetActive(true);
        }
        IEnumerator pewActive2() {
            yield return new WaitForSeconds(0.3f);
            pewObject.SetActive(true);
        }
    }
}
