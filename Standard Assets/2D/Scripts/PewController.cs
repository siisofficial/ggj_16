﻿using System;
using System.Collections;
using UnityEngine;
using UnityStandardAssets;


namespace UnityStandardAssets._2D {

    public class PewController : MonoBehaviour {
        [SerializeField]
        private float pewSpeed;


        private float pewLife = 3; //PlatformerCharacter2D.coolDown - 1 olmalı..

        private float pewPassedTime = 0;

        public Vector3 initDiff;

        private PlatformerCharacter2D characterScript;
        private string ThisName;

        public GameObject character;
        public GameObject targetCharacter;

        [SerializeField]
        private CharacterSettings characterSetting;

        private Vector3 enabledDirection;

        public GameObject onDisabledShow;


        void OnEnable() {
            //pew'ı karakterler aynı yere getirmece..
            gameObject.transform.position = character.transform.position + initDiff;
            gameObject.transform.localScale = new Vector3(
                    gameObject.transform.localScale.x * character.transform.localScale.x,
                    gameObject.transform.localScale.y,
                    gameObject.transform.localScale.z);
            enabledDirection = characterScript.direction;
        }

        void Awake() {
            characterSetting = character.GetComponent<CharacterSettings>();

            characterScript = character.GetComponent<PlatformerCharacter2D>();

            ThisName = gameObject.name.Replace("Pew", "");

        }


        // Update is called once per frame
        void Update () {

            pewPassedTime += Time.deltaTime;
            if (pewPassedTime > pewLife) {
                Disable();
                pewPassedTime = 0;
            }


            /* if (gameObject.activeSelf && characterSetting.ThisHero != CharacterSettings.Hero.Ibrahim) {
                 //Karakter'in son yönü alınır..

                 Vector3 tempVector = new Vector3(
                         (gameObject.transform.position.x - Time.deltaTime * pewSpeed * enabledDirection.x * 1.1f),
                         (gameObject.transform.position.y - Time.deltaTime * pewSpeed * enabledDirection.y * 1.1f),
                         gameObject.transform.position.z);

                 gameObject.transform.position = tempVector;


             }*/

        }


        void LateUpdate() {

            Vector3 diffVector = gameObject.transform.position - targetCharacter.transform.position;

            diffVector = diffVector.normalized;

            Vector3 tempVector = new Vector3(
                    (gameObject.transform.position.x - Time.deltaTime * pewSpeed * diffVector.x * 0.9f),
                    (gameObject.transform.position.y - Time.deltaTime * pewSpeed * diffVector.y * 0.9f),
                    gameObject.transform.position.z);

            gameObject.transform.position = tempVector;



        }

        void OnTriggerEnter(Collider GO) {
            if ((GO.name == "Mustafa" || GO.name == "Alper" || GO.name == "Ibrahim") && GO.name != ThisName) {
                Disable();
            }
        }


        void Disable() {
            if (onDisabledShow != null) {
                onDisabledShow.transform.position = gameObject.transform.position;
                onDisabledShow.SetActive(true);
            }
            gameObject.SetActive(false);
            pewPassedTime = 0;
        }



    }


}

