using System;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

namespace UnityStandardAssets._2D {
    [RequireComponent(typeof (PlatformerCharacter2D))]
    public class Platformer2DUserControl : MonoBehaviour {

        public bool player0;

        private PlatformerCharacter2D m_Character;
        private SpriteRenderer SR;
        //        private bool m_Jump;


        private void Awake() {
            m_Character = GetComponent<PlatformerCharacter2D>();
            SR = GetComponent<SpriteRenderer>();
            SR.sortingOrder = -Mathf.RoundToInt(transform.position.y * 10);
        }


        private void Update() {
            SR.sortingOrder = -Mathf.RoundToInt(transform.position.y * 10);

            /*if (!m_Jump)
            {
                // Read the jump input in Update so button presses aren't missed.
                m_Jump = CrossPlatformInputManager.GetButtonDown("Jump");
            }*/


            if (player0) {
                if (CrossPlatformInputManager.GetButtonDown("Fire1") || CrossPlatformInputManager.GetButtonDown("Fire1_jp")) {
                    m_Character.Fire1();
                }
                if (CrossPlatformInputManager.GetButtonDown("Fire2") || CrossPlatformInputManager.GetButtonDown("Fire2_jp")) {
                    m_Character.Fire2();
                }
            } else {
                if (CrossPlatformInputManager.GetButtonDown("Fire1_2") || CrossPlatformInputManager.GetButtonDown("Fire1_jp2")) {   //  Player 2
                    m_Character.Fire1();
                }
                if (CrossPlatformInputManager.GetButtonDown("Fire2_2") || CrossPlatformInputManager.GetButtonDown("Fire2_jp2")) {   //  Player 2
                    m_Character.Fire2();
                }
            }
        }


        private void FixedUpdate() {

            float horizontalMove;
            float verticalMove;

            float keyboardHorizontalP1 = CrossPlatformInputManager.GetAxis("Horizontal");
            float joypadHorizontalP1 = CrossPlatformInputManager.GetAxis("Horizontal_jp");

            float keyboardVerticalP1 = CrossPlatformInputManager.GetAxis("Vertical");
            float joypadVerticalP1 = CrossPlatformInputManager.GetAxis("Vertical_jp");

            float keyboardHorizontalP2 = CrossPlatformInputManager.GetAxis("Horizontal2");
            float joypadHorizontalP2 = CrossPlatformInputManager.GetAxis("Horizontal_jp2");

            float keyboardVerticalP2 = CrossPlatformInputManager.GetAxis("Vertical2");
            float joypadVerticalP2 = CrossPlatformInputManager.GetAxis("Vertical_jp2");


            if (player0) {
                horizontalMove = (joypadHorizontalP1 != 0 ? joypadHorizontalP1 : keyboardHorizontalP1);
                verticalMove = (joypadVerticalP1 != 0 ? joypadVerticalP1 : keyboardVerticalP1);
            } else {
                horizontalMove = (joypadHorizontalP2 != 0 ? joypadHorizontalP2 : keyboardHorizontalP2);
                verticalMove = (joypadVerticalP2 != 0 ? joypadVerticalP2 : keyboardVerticalP2);
            }

            // Pass all parameters to the character control script.
            m_Character.Move(horizontalMove, verticalMove);

        }
    }
}
