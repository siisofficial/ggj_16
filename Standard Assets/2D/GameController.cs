﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

namespace UnityStandardAssets._2D {
    public class GameController : MonoBehaviour {

        public bool LevelFinished = false;

        public Animator player1Animator;
        public Animator player2Animator;
        public PlatformerCharacter2D player1;
        public PlatformerCharacter2D player2;

        public GameObject cvs;

        public void FinishGame() {
            LevelFinished = true;
            cvs.SetActive(false);
            if (!player1.isDead) {
                player1Animator.SetTrigger("victory");
            }
            if (!player2.isDead) {
                player2Animator.SetTrigger("victory");
            }

            StartCoroutine("NextScene");
        }

        IEnumerator NextScene() {
            yield return new WaitForSeconds(4);
            int next=0;
            if (player1.isDead) {
                if(player1.CharacterSetting.ThisHero == CharacterSettings.Hero.Ibrahim && player2.CharacterSetting.ThisHero == CharacterSettings.Hero.Mustafa) {
                    next=4; //
                } else if(player1.CharacterSetting.ThisHero == CharacterSettings.Hero.Alper && player2.CharacterSetting.ThisHero == CharacterSettings.Hero.Mustafa) {
                    next=3; //
                } else if(player1.CharacterSetting.ThisHero == CharacterSettings.Hero.Alper && player2.CharacterSetting.ThisHero == CharacterSettings.Hero.Ibrahim) {
                    next=4; //
                }
            } else if(player2.isDead) {
                if(player1.CharacterSetting.ThisHero == CharacterSettings.Hero.Ibrahim && player2.CharacterSetting.ThisHero == CharacterSettings.Hero.Mustafa) {
                    next=4; //
                } else if(player1.CharacterSetting.ThisHero == CharacterSettings.Hero.Alper && player2.CharacterSetting.ThisHero == CharacterSettings.Hero.Mustafa) {
                    next=2; //
                } else if(player1.CharacterSetting.ThisHero == CharacterSettings.Hero.Alper && player2.CharacterSetting.ThisHero == CharacterSettings.Hero.Ibrahim) {
                    next=4; //
                }
            }

            Debug.Log(next);
            if(next!=0) {
                SceneManager.LoadScene(next);
            } else {
                Debug.Log("Yanlışlık Oldu");
            }
        }
    }
}
