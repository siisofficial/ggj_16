﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class pageController : MonoBehaviour {

    public Sprite[] sprites;
    public Image img;

    public bool baslat;

    public int i = 0;

    public void NextSprite() {

        if (i == sprites.Length-1) {
            i = 0;
            if (baslat) {
                SceneManager.LoadScene(1);
            }
            return;
        }
        ++i;
        img.sprite = sprites[i];
    }

    public void PrevSprite() {

        if (i == 0) {
            return;
        }
        --i;
        img.sprite = sprites[i];
    }

    void Update() {
        if (Input.GetKeyDown(KeyCode.Return) || Input.GetButtonDown("Next")) {
            NextSprite();
        }
        if (Input.GetKeyDown(KeyCode.Backspace) || Input.GetButtonDown("Back")) {
            PrevSprite();
        }
    }
}
