﻿using UnityEngine;
using System.Collections;

public class SpriteOrderer : MonoBehaviour {

    // Use this for initialization
    void Start () {
        GameObject[] Sprites;

        Sprites = GameObject.FindGameObjectsWithTag("SortThis");

        foreach (GameObject go in Sprites) {
            SpriteRenderer sr = go.GetComponent<SpriteRenderer>();
            float valY = go.GetComponent<Transform>().position.y;

            sr.sortingOrder = -Mathf.RoundToInt(valY*10);
        }
    }
}
